﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : MonoBehaviour {
	float moveSpeed, maxHealth, health;
	Grid grid;
	//armor,regen,other special properties, targets,end bases,tower lists,	players, attack method, switch ai
	float weaponDamage, weaponRange;
	Pathfinder pathfinder;
	public enum targetStateEnum{targetBase,targetPlayer,targetTurret,attackingBase}
	targetStateEnum state = targetStateEnum.targetBase;
	int currentPathIndex = 0;
	List<Vector2> path;
	public Vector3 moveTarget;
	List<GameObject> players;
	GameObject endBase;
	Vector3 playerPos;
	// Use this for initialization
	void Start () {
		players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));
		grid = GameObject.Find("A*").GetComponent<Grid>();
		pathfinder = new Pathfinder(grid);
		endBase = GameObject.Find ("EndZone");
		playerPos = players [0].transform.position;
		updateState ();
		updatePath ();
	}

	// Update is called once per frame
	void Update () {
		updateState ();
		stateActions ();
		//if player moves or (add later if a tower is placed down) update path
		if (players [0] != null) {
			if (playerPos != players [0].transform.position) {
				updatePath ();
			}
			followPath ();
			playerPos = players [0].transform.position;
		}
	}
	void OnCollisionEnter(Collision collision){
		updatePath ();
	}
	//COULDNT GET THIS TO WORK AS THEY RAN INTO WALLS AND GOT STUCK SOMETIMES ALTHOUGHT THE OTHER CODE DOES THE SAME SO NOT SURE WHAT TO DO HERE
	//also if you want to try this and cant figure out the variables i havent included here i have a vs backed up so ask me and ill send u how i had it set up
/*	void LookAhead(){
		bool wayClear = true;
		lookIndex = currentPathIndex;
		RaycastHit hitObstacle;
		Vector3 prevTarget = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);;
		//while there are no obstacles between the current pathpoint and the pathpoint we are looking ahead to...
		while (wayClear) {
			//get movetarget
			moveTarget = grid.GetWorldPos((int)path[lookIndex].x, (int)path[lookIndex].y);
			moveTarget = new Vector3 (moveTarget.x, transform.position.y, moveTarget.z); //make movetarget.y the same as enemypos.y
			//setup the ray checking at foot level of enemies for obstacles ahead
			Vector3 rayOrigin = new Vector3 (transform.position.x, transform.position.y - height / 3, transform.position.z);
			Vector3 rayTarget = new Vector3 (moveTarget.x, transform.position.y - height / 3, moveTarget.z);
			Vector3 rayDir = rayTarget - rayOrigin;
			rayDir.Normalize ();

			//if obstacle ahead change movetarget to previous movetarget, change currentPathIndex to lookindex and return
			if (Physics.Raycast (rayOrigin, rayDir, out hitObstacle, Vector3.Distance (rayTarget, rayOrigin))) {
				moveTarget = prevTarget;
				currentPathIndex = lookIndex-1;
				wayClear = false;
				return;
			}


			//else increment lookindex and set prevTarget
			lookIndex++;
			prevTarget = moveTarget;
		}
	}*/
	void updateState(){
		if (state != targetStateEnum.targetPlayer) {
			if (state != targetStateEnum.attackingBase && Vector3.Distance (endBase.transform.position, transform.position) < weaponRange) {
				state = targetStateEnum.attackingBase;
			} else {
				state = targetStateEnum.targetBase;
			}
		}
	}
	void stateActions(){
		switch (state) {
		case targetStateEnum.targetBase:
			break;
		case targetStateEnum.attackingBase:
			Explode ();
			break;
		}
	}
	void Explode() {
		var exp = GetComponent<ParticleSystem>();
		exp.Play();
		Destroy(gameObject, exp.duration);
	}
	public void setAtributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e){
		this.weaponRange = weaponRange;
		this.moveSpeed = moveSpeed;
		this.health = maxHealth = health;
		this.weaponDamage = weaponDamage;
		state = e;
		Start ();
		updatePath();
		calculateMoveTarget();
	} 
	//when this enemy takes damage returns true if it died 
	public bool RemoveHealth(int hitAmount){
		health -= hitAmount;
		if(health <= 0){
			deathActions();
			return true;
		}
		return false;
	}
	private void deathActions(){
		//stuff

		Destroy (gameObject);
	}
	private void followPath(){
		if(path != null && currentPathIndex < path.Count){
			if(moveTarget != transform.position){
				calculateMoveTarget();
			}
			else{
				currentPathIndex++;
				calculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	private void calculateMoveTarget(){
		moveTarget = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	public void updatePath(){
		switch(state){
		case targetStateEnum.targetPlayer:
			updatePathP (players);
			break;
		case targetStateEnum.targetBase:
		default:
			updatePath (endBase);
			break;
		}
	}
	private void updatePath(GameObject target){
		path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (target.transform.position));
		currentPathIndex = 0;
	}
	//calculate best player to target
	//overide this method for smarter behievour
	private void updatePathP(List<GameObject> targets){
		updatePath (targets [0]);
	}
}
